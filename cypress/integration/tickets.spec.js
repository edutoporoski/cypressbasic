describe("Tickets", () => {

    beforeEach(() => cy.visit("https://bit.ly/2XSuwCW"));
    it("fills all the text input fields", () => {
        const firstName = "Eduardo";
        const lastName = "Toporoski";
        cy.get("#first-name").type("Eduardo");
        cy.get("#last-name").type("Toporoski");
        cy.get("#email").type("testes@topo.com");
        cy.get("#requests").type("Carnivoro");
        cy.get("#signature").type(`${firstName} ${lastName}`);

    });

    it("select two tickets", () => {
        cy.get("#ticket-quantity").select("2");
    });
    it("select vip ticket type", () => {
        cy.get("#vip").check();
    });
    it("select checkbox", () => {
        cy.get("#friend").check();
        cy.get("#publication").check();
        cy.get("#friend").uncheck();

    });



    it("Has 'TICKET'headers heading", () => {
        cy.get("header h1").should("contain", "TICKETBOX");

    });

    it("alerts on invalid email", () => {
        cy.get("#email")
            .as("email")
            .type("testestests.com.br");


        cy.get("#email.invalid")
            //.as("invalidEmail")
            .should("exist");

        cy.get("@email")
            .clear()
            .type("teste@teste.com.br");

        cy.get("#email.invalid")
            .should("not.exist");
    });

    it("fills and reset the form", () => {
        const firstName = "Eduardo";
        const lastName = "Toporoski";
        const fullName = `${firstName} ${lastName}`;

        cy.get("#first-name").type("Eduardo");
        cy.get("#last-name").type("Toporoski");
        cy.get("#email").type("teste@stests.com.br");
        cy.get("#ticket-quantity").select("2");
        cy.get("#vip").check();
        cy.get("#friend").uncheck();
        cy.get("#requests").type("Carnivoro");

        cy.get(".agreement p").should(
            "contain",
            `I, ${fullName}, wish to buy 2 VIP tickets.`
        );

        cy.get("#agree").check();

        cy.get("#signature").type(fullName);

        cy.get("button[type='submit']")
            .as("submitButton")
            .should("not.be.disabled");

        cy.get("button[type='reset']").click();

        cy.get("button[type='submit']")
            .as("submitButton")
            .should("be.disabled");
    });

    it("fills mandatory fields using support command", () => {
        const customer = {
            firstName: "Joao",
            lastName: "Silva",
            email: "joaosilva@example.com"
        };

        cy.fillMandatoryFields(customer);

        cy.get("button[type='submit']")
            .as("submitButton")
            .should("not.be.disabled");
            cy.get("#agree").uncheck();

            cy.get("button[type='submit']")
                .as("submitButton")
                .should("be.disabled");

    });

});